from websockets.exceptions import ConnectionClosed
from aoiklivereload import LiveReloader
import asyncio, itertools, pickle
from datetime import datetime
from pprint import pprint

def log_msg(msg, sender, inout="<"):
    now = str(datetime.now())[:-1]
    info = "[{}] [{}] [{}] {}".format(now, inout, sender, str(msg)[0:41])
    print(info)

class WSMidiServer:
    def __init__(self):
        self.clients = set()
        self.msg = []
        self.active = 0

    async def add_consumer(self, ws):
        if ws not in self.clients:
            self.clients.add(ws)
            log_msg("Joined", ws.name, "|")
        if self.active:
            msg = ""
            while msg != "1":
                msg = await ws.recv()
        else:
            await self.consumer()



    async def producer(self, ws):
        log_msg("Connected sender", ws.name, "|")
        while True:
            try:
                msg = await ws.recv()
                self.msg = msg
                log_msg(str(msg), ws.name, "<")
            except Exception as e:
                log_msg(repr(e), ws.name, "[X]")
                self.leave(ws)
                break

    async def consumer(self):
        self.active = True
        while self.clients:
            if self.msg:
                msg = self.msg
                self.msg = None
                for ws in self.clients:
                    try:
                        print(ws.name)
                        await ws.send(msg)
                        log_msg(str(pickle.loads(msg)), ws.name, ">")
                    except Exception as e:
                        print(repr(e))
                        self.leave(ws)
            await asyncio.sleep(0.00001)
        self.active = False



import asyncio
import websockets

class wsServer:
    def __init__(self, *args, **kwargs):
        self.clients = set()
        self.msg = []
        self.active = 0
        pprint(self.__dict__)
        
    async def handler(self, ws, path):
        if ws not in self.clients: 
            ws.name = repr(ws)[-10:-1]
            self.clients.add(ws)
            log_msg("Joined", ws, "|")
        c = asyncio.ensure_future(self.consumer(ws, path))
        p = asyncio.ensure_future(self.producer(ws, path))
        done, pending = await asyncio.wait([c, p],return_when=asyncio.FIRST_COMPLETED,)
        for task in pending:
            task.cancel()

    async def producer(self, ws, path):
        while True:
            try:
                self.msg = await ws.recv()
                log_msg(str(self.msg), ws.name, "<")
            except Exception as e:
                print(repr(e))
                self.leave(ws)

            
    async def consumer(self, ws, path):
        self.active = True
        while self.clients:
            if self.msg:
                msg = self.msg
                self.msg = None
                for ws in self.clients:
                    try:
                        await ws.send(msg)
                        log_msg(str(pickle.loads(msg)), ws.name, ">")
                    except Exception as e:
                        print(repr(e))
                        self.leave(ws)
                        break
            await asyncio.sleep(0.00001)
        self.active = False

    def leave(self, ws):
        self.clients.discard(ws)
        log_msg("Removed", ws.name, "X")

if __name__ == "__main__":
    w = wsServer()
    start_server = websockets.serve(w.handler, 'localhost', 8000)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

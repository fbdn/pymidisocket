import asyncio, websockets, pickle, mido, time
from pprint import pprint
from lomond.websocket import WebSocket
from wstools import *


class WSToLocal:
    def __init__(self, target, *args, **kwargs):
        self.target = target
        self.ws = WebSocket(target)
        self.port = mido.open_output(self.__class__.__name__, virtual=True, autoreset=True)
        pprint(self.__dict__)
        

    async def recv(self):
        for msg in self.ws:
            log_msg(msg.name+str(type(msg)), self.__class__.__name__, "<")
            if msg.name == "binary":
                note = try_stuff(pickle.loads, msg.data)
                try_stuff(self.port.send, note)


class LocalToWS:
    def __init__(self, target, *args, **kwargs):
        self.target = target
        self.ws = WebSocket(target)
        self.port = mido.open_input(self.__class__.__name__, virtual=True, autoreset=True)
        pprint(self.__dict__)

    async def forward(self):
        for event in self.ws:
            if event.name == "ready":
                log_msg("Sending", self.__class__.__name__, ">")
                for note in self.port:
                    log_msg(str(note), self.__class__.__name__, ">")
                    self.ws.send_binary(pickle.dumps(note))


def l2ws(ip):
    local2ws = LocalToWS('ws://%s:8000/in' % ip)
    l = asyncio.ensure_future(local2ws.forward())
    loop = asyncio.get_event_loop()
    loop.run_forever()

def ws2l(ip):
    ws2local = WSToLocal('ws://%s:8000/feed' % ip)
    w = asyncio.ensure_future(ws2local.recv())
    loop = asyncio.get_event_loop()
    loop.run_forever()


def both(ip):
    ws2local = WSToLocal('ws://%s:8000/feed' % ip)
    local2ws = LocalToWS('ws://%s:8000/in' % ip)
    l = asyncio.ensure_future(local2ws.forward())
    w = asyncio.ensure_future(ws2local.recv())
    loop = asyncio.get_event_loop()
    loop.run_forever(asyncio.gather(l,w))

from aoiklivereload import LiveReloader
from sanic import Sanic
import asyncio, itertools, pickle
from wstools import *

class WSMidiServer:
    def __init__(self):
        self.clients = set()
        self.msg = []
        self.active = 0

    async def add_consumer(self, ws):
        if ws not in self.clients:
            self.clients.add(ws)
            log_msg("Joined", ws.name, "|")
        if self.active:
            msg = ""
            while msg != "1":
                msg = await ws.recv()
        else:
            await self.consumer()

    def leave(self, ws):
        self.clients.discard(ws)
        log_msg("Removed", ws.name, "X")

    async def producer(self, ws):
        log_msg("Connected sender", ws.name, "|")
        while True:
            try:
                msg = await ws.recv()
                self.msg = msg
                log_msg(str(msg), ws.name, "<")
            except Exception as e:
                log_msg(repr(e), ws.name, "[X]")
                self.leave(ws)
                break

    async def consumer(self):
        self.active = True
        while self.clients:
            if self.msg:
                msg = self.msg
                self.msg = None
                for ws in self.clients:
                    try:
                        await ws.send(msg)
                        log_msg(str(pickle.loads(msg)), ws.name, ">")
                    except Exception as e:
                        log_msg(repr(e), ws.name, "[X]")
                        self.leave(ws)
            await asyncio.sleep(0.00001)
        self.active = False



def setup_server(app):
    @app.websocket('/in')
    async def recieve(request, ws):
        ws.name = str(ws)[-10:-1]
        await f.producer(ws)

    @app.websocket('/feed')
    async def feed(request, ws):
        ws.name = str(ws)[-10:-1]
        await f.add_consumer(ws)


if __name__ == '__main__':
    LiveReloader().start_watcher_thread()
    app = Sanic(__name__)
    f = WSMidiServer()
    setup_server(app)
    srvr = app.create_server(host="0", debug=True)
    loop = asyncio.get_event_loop()
    srvr = asyncio.ensure_future(srvr)
    loop.run_forever()

from datetime import datetime 

def log_msg(msg, sender, inout="?", cls=None):
    now = str(datetime.now())[:-1]
    info = "[{}] [{}] [{}] {}".format(now, sender, inout, str(msg)[0:45])
    print(info)


def try_stuff(f, *args, **kwargs):
    print("try:", f.__name__,str(*args)[0:45], kwargs)
    if args and kwargs:
        try:
            return f(*args, **kwargs)
        except Exception as e:
            print(repr(e))
    if args:
        try:
            return f(*args)
        except Exception as e:
            print(repr(e))
    if kwargs:
        try:
            return f(**kwargs)
        except Exception as e:
            print(repr(e))

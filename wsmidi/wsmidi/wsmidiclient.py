import asyncio, websockets, pickle, mido, time
from pprint import pprint
from wstools import *


class wsMidiClient:
    def __init__(self, target, *args, **kwargs):
        self.target = target
        self.out = mido.open_output(self.__class__.__name__, virtual=True, autoreset=True)
        self.in_port = mido.open_input(self.__class__.__name__, virtual=True, autoreset=True)
        pprint(self.__dict__)
        
    async def handler(self):
        async with websockets.connect(self.target) as ws:
            consumer_task = asyncio.ensure_future(self.consumer(ws))
            producer_task = asyncio.ensure_future(self.producer_handler(ws))
            done, pending = await asyncio.wait([consumer_task, producer_task],return_when=asyncio.FIRST_COMPLETED,)
            for task in pending:
                task.cancel()


    async def consumer(self, ws):
        while True:
            msg = await ws.recv()
            log_msg(str(msg), self.__class__.__name__, "<")
            note = try_stuff(pickle.loads, msg.data)
            try_stuff(self.port.send, note)
            
    async def producer_handler(self, ws):
        while True:
            for note in self.in_port:
                log_msg(str(note), self.__class__.__name__, ">")
                await ws.send(pickle.dumps(note))


if __name__ == "__main__":
    ws = wsMidiClient("ws://0:8000/").handler()
    asyncio.get_event_loop().run_until_complete(ws)
    asyncio.get_event_loop().run_forever()

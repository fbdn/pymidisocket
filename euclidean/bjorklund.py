#!/usr/bin/env python3

def bjorklund(steps, pulses):
    steps = int(steps)
    pulses = int(pulses)
    if pulses > steps:
        raise ValueError    
    pattern = []    
    counts = []
    remainders = []
    divisor = steps - pulses
    remainders.append(pulses)
    level = 0
    while True:
        counts.append(divisor // remainders[level])
        remainders.append(divisor % remainders[level])
        divisor = remainders[level]
        level = level + 1
        if remainders[level] <= 1:
            break
    counts.append(divisor)
    
    def build(level):
        if level == -1:
            pattern.append(0)
        elif level == -2:
            pattern.append(1)         
        else:
            for i in range(0, counts[level]):
                build(level - 1)
            if remainders[level] != 0:
                build(level - 2)
    
    build(level)
    i = pattern.index(1)
    pattern = pattern[i:] + pattern[0:i]
    return pattern

if __name__ == "__main__":
    from itertools import starmap

    a = bjorklund(16, 5)
    print(a, len(a))
    b = bjorklund(16,8)
    print(b, len(b))
    c = bjorklund(13,5)
    print(c, len(c))

    patterns=[(16,8), (16,16), (16,6), (16,3), (16,12)]
    patterns = starmap(bjorklund, patterns) # Starmaps are useless
    patterns = [p for p in patterns] # Convert back to list

    for pattern in patterns:
        pattern_new = [60+(7*patterns.index(pattern)) if n==1 else 0 for n in pattern]
        patterns[patterns.index(pattern)] = pattern_new
        print(pattern_new, len(pattern_new))


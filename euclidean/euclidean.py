import time, mido, asyncio, os
from my_tools import to_thread,  yamlify, name
from itertools import starmap, cycle
from bjorklund import bjorklund


class Rhythm:
    def __init__(self, pattern, note=60, shift=0, beats=1, port=0):
        self.port = port if port else mido.open_output(
                "{name(self)}_{self.pattern}", 
                virtual=True, 
                autoreset=True)

        self.pattern = pattern
        self.shift = shift
        self.beats = beats
        self.note = note

        self.on = on.copy(note = self.note)
        self.off = off.copy(note = self.note)
        self.euclid_pattern = bjorklund(self.pattern[0], self.pattern[1])
        self.euclid_pattern = [self.note if n==1 else 0 for n in self.euclid_pattern]
        
        # if self.shift: 
            # self.euclid_pattern = rotate(self.euclid_pattern, self.shift)
        print(self.euclid_pattern)

    @to_thread
    def send(self, i, wait, beats=1, port=0):
        current_note = self.euclid_pattern[i]
        if current_note == 0: return
        self.port.send(self.on)
        time.sleep(wait*beats)
        self.port.send(self.off)

    def __repr__(self):
        return f"{self.pattern}"


class PatternHost:
    def __init__(self, bpm, rhythms=None):
        self.BPM = bpm
        self.rhythms = rhythms
        self.WAIT = 60.0 / bpm
        print(self.rhythms)
        print(mido.get_output_names())

    def run_gen(self):
        while True:
            for beat in range(16):
                print(beat)
                for rhythm in self.rhythms:
                    rhythm.send(i=beat, wait=self.WAIT)
                    print(rhythm)
                time.sleep(self.WAIT)


if __name__ == "__main__":
    mido.set_backend('mido.backends.rtmidi/UNIX_JACK')
    on = mido.Message('note_on', note=60)
    off = mido.Message('note_off', note=60)

    # port = mido.open_output("Euclid", virtual=True, autoreset=True)

    patterns = [(16,8), (16,5), (16,6), (16,3), (16,12), (16,16)]
    pats = [Rhythm(pattern=p, note=60+i*3, port=0) for i,p in enumerate(patterns)]

    p = PatternHost(175, rhythms=pats)
    p.run_gen()

    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(p.run_gen())

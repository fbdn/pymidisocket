def to_thread(f):
    def wrapper(self=None, *args, **kwargs):
        thread_args = []
        if self is not None: thread_args = [self, ]
        if any(args): thread_args = tuple(thread_args + [args])
        # print(thread_args, kwargs)
        bg_thread = Thread(
                name = "{}.{}".format(self, f.__name__),
                args =  thread_args,
                kwargs = dict(kwargs),
                target = f)

        bg = bg_thread.start()
        # logging.debug("{}, {}, {}".format(bg_thread, args, kwargs))
        return bg # or f, not sure -.-
    return wrapper

